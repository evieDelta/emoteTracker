module codeberg.org/eviedelta/emotetracker

go 1.14

require (
	codeberg.org/eviedelta/drc v0.0.0-20200809192307-91261482c012
	codeberg.org/eviedelta/trit v0.0.0-20200522120239-e627923758e3
	github.com/burntsushi/toml v0.3.1
	github.com/bwmarrin/discordgo v0.22.0
	github.com/fogleman/gg v1.3.0
	github.com/gocraft/dbr/v2 v2.7.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/lib/pq v1.8.0
	github.com/pkg/errors v0.9.1
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76 // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e
)
