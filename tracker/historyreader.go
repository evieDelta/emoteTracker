package tracker

import (
	"log"
	"time"

	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"golang.org/x/time/rate"
)

// NOTE: this was supposed to be an attempt to read message history and be able to skip the wait of waiting for data to slowly be collected
// however as it is in its current state it does not work reliably and it should not be trusted
// the main function works, however my attempt to make it not fail completely on a single error did not, leading to a messy situation with the error handling
// this code is also undocumented so goodluck to anybody who tries to delve into this

// i might try to fix it later, but i'm trying to get the other parts working right now

// DScanserver is beta
var DScanserver = &drc.Command{
	Name:         "scanserver",
	Manual:       []string{"scans all the past messages in the server"},
	CommandPerms: discordgo.PermissionSendMessages,
	Permissions: drc.Permissions{
		BotAdmin: trit.True,
	},
	Config: drc.CfgCommand{
		DataFlags: map[string]string{
			"until": "0",
		},
		Listable: false,
		ReactOn: drc.ActOn{
			Success: trit.True,
		},
		MinimumArgs: 0,
	},
	Exec: fScanserver,
}

func fScanserver(ctx *drc.Context) error {
	guild, err := ctx.Ses.State.Guild(ctx.Mes.GuildID)
	if err != nil {
		return err
	}
	until, err := ctx.Flags["until"].Int64()
	if err != nil {
		return err
	}

	var total, msgct int

	var errorcount = rate.NewLimiter(rate.Every(time.Second), 5)
	var errs manyError

	for _, c := range guild.Channels {
		t, m, err := loadAllEmotesFromChannel(ctx.Ses, c.ID, time.Unix(until, 0))
		if err != nil {
			log.Println(err)
			errs.errs = append(errs.errs, errors.Wrap(err, "loadAllEmotesFromChannel"))
			if !errorcount.Allow() {
				return errs
			}
		}
		total += t
		msgct += m
	}

	ctx.Replyf("Added %v message emotes to the database from %v messages", total, msgct)
	return errs
}

// DScanchannel is beta
var DScanchannel = &drc.Command{
	Name:         "scanchannel",
	Manual:       []string{"scans all the past messages in the attached channel"},
	CommandPerms: discordgo.PermissionSendMessages,
	Permissions: drc.Permissions{
		BotAdmin: trit.True,
	},
	Config: drc.CfgCommand{
		DataFlags: map[string]string{
			"until": "0",
		},
		Listable: false,
		ReactOn: drc.ActOn{
			Success: trit.True,
		},
		MinimumArgs: 1,
	},
	Exec: fScanchannel,
}

func fScanchannel(ctx *drc.Context) error {
	channel := ""
	ch, ok, err := ctx.Args[0].Channel(ctx)
	if err != nil && ok {
		return err
	}
	if !ok {
		channel = ctx.Mes.ChannelID
	} else {
		channel = ch.ID
	}

	until, err := ctx.Flags["until"].Int64()
	if err != nil {
		return err
	}

	total, msgct, err := loadAllEmotesFromChannel(ctx.Ses, channel, time.Unix(until, 0))
	if err != nil {
		log.Println(err)
	}

	ctx.Replyf("Added %v message emotes to the database from %v messages", total, msgct)
	return err
}

func loadAllEmotesFromChannel(s *discordgo.Session, channel string, until time.Time) (total, msgct int, err error) {
	msgs, err := fetchAllFromChannel(s, channel, until, 10000000)
	if err != nil {
		return 0, 0, errors.Wrap(err, "fetchAllFromChannel")
	}

	var errorcount = rate.NewLimiter(rate.Every(time.Second), 5)
	var errs manyError

	for _, m := range msgs {
		if m.GuildID == "" {
			ch, err := s.State.Channel(m.ChannelID)
			if err != nil {
				return 0, 0, err
			}
			m.GuildID = ch.GuildID
		}
		i, err := handleMessage(m, false)
		if err != nil {
			log.Println(err)
			errs.errs = append(errs.errs, errors.Wrap(err, "handleMessage"))
			if !errorcount.Allow() {
				return 0, 0, errs
			}
		}

		total += i
	}

	return total, len(msgs), err
}

func fetchAllFromChannel(s *discordgo.Session, channel string, until time.Time, capacity int) ([]*discordgo.Message, error) {
	var last string
	var errorcount = rate.NewLimiter(rate.Every(time.Second), 5)
	var errs manyError
	var msgs = make([]*discordgo.Message, 0, capacity)

messagegobbler:
	for i := 0; i < capacity/100; i++ {
		m, err := s.ChannelMessages(channel, 100, last, "", "")
		if err != nil {
			log.Println(err)
			errs.errs = append(errs.errs, errors.Wrap(err, "session.ChannelMessages"))
			if !errorcount.Allow() {
				return msgs, errs
			}
			time.Sleep(time.Second)
		}
		last = m[len(m)-1].ID
		for _, x := range m {
			msgs = append(msgs, x)

			t, err := discordgo.SnowflakeTimestamp(x.ID)
			if err != nil {
				// i feel like the error description here describes my mood when writing this
				errs.errs = append(errs.errs, errors.Wrap(err, "dgi,SbiwfkajeTunestano"))
				if !errorcount.Allow() {
					return msgs, errs
				}
			} else if t.Unix() < until.Unix() {
				break messagegobbler
			}
		}
		if len(m) < 100 {
			break
		}
	}
	return msgs, nil
}

type manyError struct {
	errs []error
}

func (m manyError) Error() string {
	var s string
	for _, e := range m.errs {
		s = s + e.Error() + " | "
	}
	return s
}
