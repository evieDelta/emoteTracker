package tracker

import (
	"strings"
	"time"

	"codeberg.org/eviedelta/emotetracker/wlog"
	"github.com/bwmarrin/discordgo"
)

// reactionAdd calls handleNewReaction on a reaction event
func reactionAdd(s *discordgo.Session, r *discordgo.MessageReactionAdd) {
	err := handleNewReaction(s, r.MessageReaction)
	if err != nil {
		wlog.Err.Print(err)
	}
}

// handleNewReaction goes through the steps and adds a reaction to the database
func handleNewReaction(s *discordgo.Session, r *discordgo.MessageReaction) error {
	if !strings.Contains(r.Emoji.APIName(), ":") {
		return nil
	}

	emo := Emote{
		ID:       r.Emoji.ID,
		Name:     r.Emoji.Name,
		Animated: r.Emoji.Animated,
	}
	emo, err := getEmote(emo, Config.SaveTo)
	if err != nil {
		return err
	}

	rec := Reaction{
		Message:   r.MessageID,
		Guild:     r.GuildID,
		Channel:   r.ChannelID,
		User:      r.UserID,
		EmoteID:   r.Emoji.ID,
		EmoteHash: emo.Hash,
		Timestamp: time.Now().UTC(),
	}

	return addReaction(rec)
}

// Function which was written when i was attempting to make a message history reader to add prior reactions,
// didn't end up working as well as hoped so, name probably describes my feelings when trying to write it
// yes is is basically an exact duplicate of the above
/* func handleNewReactionExceptItTakesRawInputLolThisIsALongNamePlsHelpImTired(
	s *discordgo.Session, emoteid, emotename string, emoteani bool, messageid, guildid, channelid, userid string,
) error {
	emo := Emote{
		ID:       emoteid,
		Name:     emotename,
		Animated: emoteani,
	}
	emo, err := getEmote(emo, Config.SaveTo)
	if err != nil {
		return err
	}

	rec := Reaction{
		Message:   messageid,
		Server:    guildid,
		Channel:   channelid,
		User:      userid,
		EmoteID:   emoteid,
		EmoteHash: emo.Hash,
		Timestamp: time.Now().UTC(),
	}

	return addReaction(rec)
}*/

// reactionRemove handles a reaction removal event
func reactionRemove(s *discordgo.Session, r *discordgo.MessageReactionRemove) {
	if !strings.Contains(r.Emoji.APIName(), ":") {
		return
	}

	err := removeReaction(r.ChannelID, r.MessageID, r.Emoji.ID, r.UserID)
	if err != nil {
		wlog.Err.Print(err)
	}
}

// reactionBulkRemove handles a bulk reaction removal event
func reactionBulkRemove(s *discordgo.Session, r *discordgo.MessageReactionRemoveAll) {
	err := removeReactionAll(r.ChannelID, r.MessageID)
	if err != nil {
		wlog.Err.Print(err)
	}
}
