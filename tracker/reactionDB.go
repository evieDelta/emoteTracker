package tracker

import (
	"time"

	"github.com/gocraft/dbr/v2"
)

// Reaction contains the information about discord message reaction events, tracked for the purpose of this bot
type Reaction struct {
	Message   string    `db:"message_id"`
	Guild     string    `db:"guild_id"`
	Channel   string    `db:"channel_id"`
	User      string    `db:"user_id"`
	EmoteID   string    `db:"emote_id"`
	EmoteHash string    `db:"emote_hash"`
	Timestamp time.Time `db:"timestamp"`
}

// addReaction inserts a new reaction into the database
func addReaction(react Reaction) error {
	_, err := ss.InsertInto("reactions").
		Columns("message_id", "guild_id", "channel_id", "user_id", "emote_id", "emote_hash", "timestamp").
		Record(&react).Exec()
	return err
}

// addReactions inserts a slice of reactions into the database
func addReactions(reacts []Reaction) error {
	for _, r := range reacts {
		err := addReaction(r)
		if err != nil {
			return err
		}
	}
	return nil
}

// getReactEmoteCountByGuild returns how many emotes have been used in reactions within a server
func getReactEmoteCountByGuild(guild string) (count int, err error) {
	i, err := ss.Select("COUNT(emote_id)").
		From("reactions").
		Where(dbr.Eq("guild_id", guild)).
		Load(&count)

	if i < 1 {
		return
	}
	return count, err
}

// getReactEmoteCountGlobal returns how many emotes have been used in messages globally
func getReactEmoteCountGlobal() (count int, err error) {
	i, err := ss.Select("COUNT(emote_id)").
		From("reactions").
		Load(&count)

	if i < 1 {
		return
	}
	return count, err
}

// removeReaction removes a single reaction by a user from the database
func removeReaction(channel, message, emoteID, user string) error {
	_, err := ss.DeleteFrom("reactions").
		Where(dbr.Eq("channel_id", channel)).
		Where(dbr.Eq("message_id", message)).
		Where(dbr.Eq("emote_id", emoteID)).
		Where(dbr.Eq("user_id", user)).
		Exec()
	return err
}

// removeReactionAll removes all reactions on a message from the database
func removeReactionAll(channel, message string) error {
	_, err := ss.DeleteFrom("reactions").
		Where(dbr.Eq("channel_id", channel)).
		Where(dbr.Eq("message_id", message)).
		Exec()
	return err
}
