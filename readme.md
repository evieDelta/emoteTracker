# emoteTracker

This is a work in progress bot for tracking emote usage within a server

Its pretty much pre-alpha so the modularisation and structure is really bad, but the main tracker part appears to work fine  
any kind of fanciness like a top emotes command or other statistics are not implemented at this point

for those who want to read this code or contribute, i'll just give a prior warning to keep in mind the code is currently an abomination  
the modularisation is largely an experimental mess, and likely large amounts of refactoring will be needed before any larger developments are done to this platform (i'm pretty much learning how to code while i design at this point)

sometime i'd like to refactor large parts of the bot to be a lot more elegant and a lot less questionable design held together with ducktape and glue (it really is a mess atm and i want to rewrite it eventually)

### development requirements
due to hosting, the bot must be able to run on a 32bit arm system (raspberry pi to be precise) and any database actions must be compatible with PostgreSQL 9.6

features from the most recent golang versions may be used though

### requirements for running the bot
* Golang 1.13 or newer (might work on older versions but not been tested)
* Postgresql 9.6 or newer (like above, might work with earlier but it has not been tested)
* A directory for configuration (the config system is pretty beta atm, so it uses .toml configs, templates are in the docs folder, by default it checks the ./ and ./data folders for configs, but this can be customised via the -data flag when running it)
* The database layout required for operation is contained in a file named database.sql in the docs folder

### Building
1. Install golang 1.13 or newer  
2. Setup the pSQL database (you'll need knowlage of how to operate posqresql for this, you can find the reference sql in docs/database.sql)  
3. Set up the configuration, examples in the docs folder  
4. Build  
> $ go build  

### Running
run the executable given by go build or use go run

## license
For the time being with the current messy state of this project i have decided to place it under the unlicense  
in the future if this project reaches a point of being much more confident and less messy i may reconsider the licensing  
however all versions/commits and the code within (unless specifically stated otherwise) that contain this notice will remain in the public domain as described in the unlicense

> This is free and unencumbered software released into the public domain.
> 
> Anyone is free to copy, modify, publish, use, compile, sell, or
> distribute this software, either in source code form or as a compiled
> binary, for any purpose, commercial or non-commercial, and by any
> means.
>
> In jurisdictions that recognize copyright laws, the author or authors
> of this software dedicate any and all copyright interest in the
> software to the public domain. We make this dedication for the benefit
> of the public at large and to the detriment of our heirs and
> successors. We intend this dedication to be an overt act of
> relinquishment in perpetuity of all present and future rights to this
> software under copyright law.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
> IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
> OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
> ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
> OTHER DEALINGS IN THE SOFTWARE.
>
> For more information, please refer to <http://unlicense.org/>