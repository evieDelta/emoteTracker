package main

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

// the thing that does the on message stuff
func onMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	// debug purposes only, logs all messages to the terminal
	// ### *** DO NOT ENABLE IN PRODUCTION ***
	if conf.Bot.Debugm {
		log.Printf("New message by %v (%v) in %v : %v\n> %v\n", m.Author.String(), m.Author.ID, m.ChannelID, m.GuildID, m.Content)
	}

	// Call message handler and print if it errors
	err := Hn.OnMessage(s, m)
	if err != nil {
		log.Println("Error: ", err)
	}
}
