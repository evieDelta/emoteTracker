package dwh

var defWebhook = NewWebhook("")

// SendMessageTo sends a message to a specified discord webhook using the default handler
func SendMessageTo(url string, message WebhookMessage) error {
	return defWebhook.pushWebhookToURL(url, message)
}
