package wlog

import (
	"codeberg.org/eviedelta/emotetracker/wlog/dwh"
	"codeberg.org/eviedelta/emotetracker/wlog/wlogger"
)

// Info is a standard logger for info
var Info = wlogger.Logger{
	Status: "Info",
	Color:  5353325,

	Webhook: dwh.NewWebhook(""),
}

// Err is a standard logger for errors
var Err = wlogger.Logger{
	Status: "Error",
	Color:  13971547,

	Webhook: dwh.NewWebhook(""),
}
